import Link from 'next/link';
import { useRouter } from 'next/router';
import { PropsWithChildren } from 'react';

export default function MarkdownLink(
  props: PropsWithChildren<{ href: string }>,
) {
  const router = useRouter();

  if (!props.href) {
    return <a>{props.children}</a>;
  }

  const { external, href } = calculateLink({
    markdownLink: props.href,
    docRoot: router.query.root as string,
    currentUrl: router.asPath,
    filePath: (router.query.filePath as string[])
      .join('/')
      .replace(/\s/g, '%20'),
  });

  return (
    <Link href={href}>
      <a target={external ? '_blank' : '_self'}>{props.children}</a>
    </Link>
  );
}

function calculateLink(args: {
  markdownLink: string;
  docRoot: string;
  currentUrl: string;
  filePath: string;
}) {
  try {
    // If this is a valid external link or ONLY an anchor has, this won't throw an error.
    // Otherwise, it will error.
    const url = new URL(args.markdownLink);

    if (url.origin === 'null') {
      throw 'This is just an anchor hash';
    }

    return {
      external: true,
      href: args.markdownLink,
    } as const;
  } catch {
    const { url, queryParams, fragment } = normalizeURL(
      args.markdownLink,
      args.currentUrl,
      args.filePath,
    );

    const query = { ...queryParams };

    if (args.docRoot) {
      query.root = args.docRoot;
    }

    return {
      external: false,
      href: {
        pathname: url.replace(/\%20/g, ' '),
        query,
        hash: fragment || undefined,
      },
    } as const;
  }
}

function normalizeURL(
  markdownLink: string,
  currentUrl: string,
  filePath: string,
) {
  const newPath = normalizeDocumentPath(filePath!, markdownLink);
  const [path, queryParams] = newPath.split('?');
  const [pathMinusFragment, fragment] = path.split('#');

  const newUrl = currentUrl
    .replace(filePath!, pathMinusFragment)
    .split('?')
    .shift()!
    .split('#')
    .shift()!;

  return {
    url: newUrl,
    queryParams: translateQueryParamStringToParamObject(queryParams),
    fragment,
  };
}

function normalizeDocumentPath(currentURL: string, newURL: string): string {
  if (newURL.startsWith('../')) {
    const piecesOP = currentURL.split('/');
    piecesOP.pop();
    // piecesOP.pop();

    const piecesNP = newURL.split('../');
    piecesNP.shift();

    return normalizeDocumentPath(piecesOP.join('/'), piecesNP.join('../'));
  } else if (newURL.startsWith('./')) {
    const piecesNP = newURL.split('../');
    piecesNP.shift();

    return normalizeDocumentPath(currentURL, newURL.slice(2));
  }

  if (newURL.startsWith('#')) {
    const pieces = currentURL.split('#');
    return `${pieces[0]}${newURL}`;
  } else {
    const pieces = currentURL.split('/');
    pieces.pop();
    return `${pieces.join('/')}/${newURL}`;
  }
}

function translateQueryParamStringToParamObject(
  text?: string,
): { [key: string]: string } | null {
  if (!text) return null;

  const keyVal: Array<[string, string]> = [];

  for (const pair of text.split(',')) {
    const pieces = pair.split('=') as [string, string];

    if (pieces.length !== 2) continue;

    keyVal.push(pieces);
  }

  return Object.fromEntries(keyVal);
}
