import Head from 'next/head';
import Header from '@/components/header';
import Sidenav from '@/components/sidenav';
import DocViewer from './doc-viewer';
import { PropsWithChildren, useState } from 'react';
import type { IProject } from '../repo/util';
import {
  capitalize,
  createStyles,
  Divider,
  makeStyles,
  Theme,
} from '@material-ui/core';
import { useRouter } from 'next/router';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: theme.mixins.toolbar,
  }),
);

export default function DocPage(
  props: PropsWithChildren<{
    project: IProject;
    article: JSX.Element;
    hrefBase: string;
  }>,
) {
  const [sidenavOpen, setSidenavOpen] = useState(false);
  const classes = useStyles();
  const router = useRouter();

  const onSidenavToggle = () => setSidenavOpen(!sidenavOpen);

  const { project, article } = props;

  const {
    projectNamespace,
    projectName,
    refType,
    ref,
    filePath: filePathPieces,
  } = router.query;

  const hrefBase = `${props.hrefBase}/${projectNamespace}/${projectName}/${refType}/${ref}`;

  const filePath = (filePathPieces as string[]).join('/');
  const { url } = project.files.find((f) => f[0] === filePath)![1];

  return (
    <>
      <Head>
        <title>Dynodocs - {project.name}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header project={project} onSidenavToggle={onSidenavToggle}></Header>
      {/* marginLeft: '200px', marginTop: '4rem' */}
      <div style={{ display: 'flex' }}>
        <Sidenav
          hrefBase={hrefBase}
          files={project.files}
          sidenavOpen={sidenavOpen}
          onSidenavToggle={onSidenavToggle}
        ></Sidenav>

        <main style={{ overflowX: 'auto' }}>
          <div className={classes.toolbar} />

          <div className="article-metadata">
            <div>
              {capitalize(refType as string)} <i>"{ref}"</i>
            </div>
            <span className="separator">|</span>
            <div>
              File{' '}
              <i>
                "<a href={url}>{filePath}</a>"
              </i>
            </div>
          </div>

          <Divider />

          <DocViewer>{article}</DocViewer>
        </main>
      </div>
    </>
  );
}
