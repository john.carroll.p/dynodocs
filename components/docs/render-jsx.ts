import { createElement } from 'react';
import unified from 'unified';
import remark2react from 'remark-react';
import MarkdownLink from './markdown-link';

const renderer = unified().use(remark2react, {
  createElement,
  sanitize: {
    attributes: {
      '*': ['className'],
      h1: ['id'],
      h2: ['id'],
      h3: ['id'],
      h4: ['id'],
      h5: ['id'],
      img: ['src'],
      a: ['href', 'target'],
    },
    clobber: ['name'],
  },
  remarkReactComponents: {
    a: MarkdownLink,
  },
});

export default function renderJSX(remark: any) {
  try {
    return (renderer.stringify(remark) as unknown) as JSX.Element;
  } catch (e) {
    console.error(e);
    throw e;
  }
}
