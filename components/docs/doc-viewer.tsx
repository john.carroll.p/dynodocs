// import { createContext, useContext } from 'react';
// import DocLoading from './doc-loading';

export default function DocViewer(props: { children: JSX.Element }) {
  return <article style={{ padding: '1.5rem' }}>{props.children}</article>;
}
