declare module 'remark-react' {
  import { Plugin } from 'unified';
  import { Options } from 'mdast-util-from-markdown';

  declare namespace remark2react {
    interface Options {
      // remarkReactComponents: { [key: string]: any };
      [key: string]: any;
    }

    interface Parse extends Plugin<[Options?]> {}
  }

  declare const remark2react: remark2react.Parse;

  export default remark2react;
}

declare module 'remark-highlight.js' {
  const val: () => any;
  export default val;
}

declare module 'remark-slug' {
  const val: () => any;
  export default val;
}

declare module 'remark-autolink-headings' {
  import { Plugin } from 'unified';
  import { Options } from 'mdast-util-from-markdown';

  declare namespace headings {
    interface Options {
      [key: string]: any;
    }

    interface Parse extends Plugin<[Options?]> {}
  }

  declare const headings: headings.Parse;

  export default headings;
}
