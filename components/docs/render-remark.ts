import unified from 'unified';
import parse from 'remark-parse';
import highlight from 'remark-highlight.js';
import sluggify from 'remark-slug';
import headings from 'remark-autolink-headings';

const renderer = unified()
  .use(parse)
  .use(highlight)
  .use(sluggify)
  .use(headings, {
    behavior: 'append',
  });

export default async function transformMarkdownToRemark(markdown: string) {
  try {
    const parsed = renderer.parse(markdown);
    return await renderer.run(parsed);
  } catch (e) {
    console.error(e);
    throw e;
  }
}
