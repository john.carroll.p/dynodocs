import { CircularProgress } from '@material-ui/core';

export default function Loading() {
  return (
    <div className="docs-loading">
      <h1> Rendering Docs... </h1>
      {/* <CircularProgress size="3rem" /> */}
    </div>
  );
}
