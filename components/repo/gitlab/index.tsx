import type { IProject } from '../util';
import { useMemo } from 'react';
import renderJSX from '../../docs/render-jsx';
import { useRouter } from 'next/router';
import DocPage from '../../docs/doc-page';
import Loading from '../loading';

export default function GitlabDoc({
  project,
  article,
}: {
  project: IProject;
  article: object;
}) {
  const router = useRouter();
  const articleElement = useMemo(
    () => (!router.isFallback && renderJSX(article)) || null,
    [router.isFallback, router.asPath],
  );

  if (router.isFallback) return <Loading />;
  if (!project || !articleElement) return <h1>ERROR!!!!</h1>;

  return (
    <DocPage
      project={project}
      article={articleElement}
      hrefBase="/d/gitlab"
    ></DocPage>
  );
}
