import transformMarkdownToRemark from '../../docs/render-remark';
import { GetStaticProps } from 'next';
import {
  PARAMS_DECODER,
  PAGE_NOT_FOUND,
  REVALIDATE_REPO_INTERVAL,
  IFileMeta,
  IProject,
  stringComparer,
  processFiles,
  normalizeFileName,
} from '../util';
import { areDecoderErrors, assert } from 'ts-decoders';
import { arrayD, numberD, objectD, stringD } from 'ts-decoders/decoders';

const apiEndpoint = `https://gitlab.com/api/v4`;
const graphqlEndpoint = `https://gitlab.com/api/graphql`;

const validateFileMeta = assert(
  arrayD(
    objectD({
      name: stringD(),
      path: stringD(),
      type: stringD(),
      webUrl: stringD(),
      sha: stringD(),
    }).map((o) => {
      const newO = {
        ...o,
        url: o.webUrl,
        displayName: normalizeFileName(o.name),
      } as IFileMeta;
      delete (newO as any)['webUrl'];
      return [newO.path, newO] as [string, typeof newO];
    }),
  ).map(processFiles),
);

const validateProject = assert(
  objectD({
    id: numberD(),
    name: stringD(),
    web_url: stringD(),
  }).map((o) => {
    const newO = { ...o, url: o.web_url } as Omit<IProject, 'files'>;
    delete (newO as any)['web_url'];
    return newO;
  }),
);

// interface IFileMeta {
//   id: number;
//   name: string;
//   path: string;
//   type: string;
//   webUrl: string;
//   sha: string;
// }

// export interface IProject {
//   id: string;
//   name: string;
//   url: string;
//   files: Array<[string, IFileMeta]>;
// }

export const getStaticProps: GetStaticProps = async (context) => {
  const result = PARAMS_DECODER.decode(context.params);

  if (areDecoderErrors(result)) return PAGE_NOT_FOUND;

  const {
    projectNamespace,
    projectName,
    filePath,
    ref,
    root: docRoot,
  } = result.value;

  const projectPath = `${projectNamespace}/${projectName}`;

  const project = await fetchProject(projectPath, docRoot, ref);

  if (!project) return PAGE_NOT_FOUND;

  const article = await fetchFile(project.id, filePath, ref);

  if (!article) return PAGE_NOT_FOUND;

  return {
    props: {
      project,
      article,
    },
    revalidate: REVALIDATE_REPO_INTERVAL,
  };
};

async function fetchProject(projectPath: string, docRoot: string, ref: string) {
  // fetch project properties
  const p1 = fetch(
    `${apiEndpoint}/projects/${projectPath.replace(/\//g, '%2F')}`,
    {
      method: 'GET',
      credentials: 'omit',
    },
  );

  // fetch project file list
  const p2 = fetch(graphqlEndpoint, {
    body: JSON.stringify({
      operationName: 'GetFiles',
      query: `
        query GetFiles($projectPath: ID!, $docRoot: String!, $ref: String!) {
          project(fullPath: $projectPath) {
            repository {
              tree(path: $docRoot, recursive: true, ref: $ref) {
                blobs {
                  nodes {
                    id
                    name
                    path
                    type
                    webUrl
                    sha
                  }
                }
              }
            }
          }
        }
      `,
      variables: {
        projectPath,
        docRoot,
        ref,
      },
    }),
    method: 'POST',
    credentials: 'omit',
    headers: {
      'Content-Type': 'application/json',
    },
  });

  const [r1, r2] = await Promise.all([p1, p2]);

  if (!r1.ok || !r2.ok) return null;

  const [j1, j2] = await Promise.all([
    r1.json(),
    r2.json() as Promise<{
      data?: {
        project?: {
          repository?: {
            tree?: {
              blobs?: {
                nodes?: IFileMeta[];
              };
            };
          };
        };
      };
    }>,
  ]);

  const project = validateProject(j1);

  const files = validateFileMeta(
    j2?.data?.project?.repository?.tree?.blobs?.nodes! || [],
  );

  return {
    ...project,
    files,
  };
}

async function fetchFile(projectId: number, filePath: string[], ref: string) {
  const path = filePath.map((p) => p.replace(/\s/g, '%20')).join('%2F');

  const url = `${apiEndpoint}/projects/${projectId}/repository/files/${path}/raw?ref=${ref}`;

  const response = await fetch(url, {
    method: 'GET',
    credentials: 'omit',
  });

  if (!response.ok) return null;

  const markdown = await response.text();

  return await transformMarkdownToRemark(markdown);
}
