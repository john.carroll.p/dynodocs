import { GetStaticProps } from 'next';
import {
  PARAMS_DECODER,
  PAGE_NOT_FOUND,
  REVALIDATE_REPO_INTERVAL,
  IFileMeta,
  IProject,
  normalizeFileName,
  processFiles,
} from '../util';
import { areDecoderErrors, assert } from 'ts-decoders';
import { arrayD, numberD, objectD, stringD } from 'ts-decoders/decoders';
import transformMarkdownToRemark from '../../docs/render-remark';

const apiEndpoint = `https://api.github.com`;
// Github does not support unauthenticated GraphQL requests :'(
// const graphqlEndpoint = `https://api.github.com/graphql`;

const validateFileMeta = assert(
  arrayD(
    objectD({
      path: stringD(),
      type: stringD(),
      url: stringD(),
      sha: stringD(),
    }).map((o) => {
      const name = o.path.split('/').pop()!;
      const newO = {
        ...o,
        name,
        displayName: normalizeFileName(name),
      } as IFileMeta;
      // delete (newO as any)['webUrl'];
      return [newO.path, newO] as [string, typeof newO];
    }),
  ).map(processFiles),
);

const validateProject = assert(
  objectD({
    id: numberD(),
    name: stringD(),
    html_url: stringD(),
  }).map((o) => {
    const newO = { ...o, url: o.html_url } as Omit<IProject, 'files'>;
    delete (newO as any)['html_url'];
    return newO;
  }),
);

// interface IFileMeta {
//   "path": "BUILD.bazel",
//   "mode": "100644",
//   "type": "blob",
//   "sha": "30175c4e7d268521467bbd04ea59bbef62a69d05",
//   "size": number,
//   "url": "https://api.github.com/repos/angular/angular/git/blobs/30175c4e7d268521467bbd04ea59bbef62a69d05"
// }

// interface IFileMeta {
//   path: string;
//   mode: string;
//   type: string;
//   sha: string;
//   size: number;
//   url: string;
//   name: string;
// }

// export interface IProject {
//   id: string;
//   name: string;
//   html_url: string;
//   files: Array<[string, IFileMeta]>;
// }

export const getStaticProps: GetStaticProps = async (context) => {
  const result = PARAMS_DECODER.decode(context.params);

  if (areDecoderErrors(result)) return PAGE_NOT_FOUND;

  const {
    projectNamespace,
    projectName,
    filePath,
    ref,
    root: docRoot,
  } = result.value;

  const projectPath = `${projectNamespace}/${projectName}`;

  const project = await fetchProject(projectPath, docRoot, ref);

  if (!project) return PAGE_NOT_FOUND;

  const article = await fetchFile(projectPath, filePath, ref);

  if (!article) return PAGE_NOT_FOUND;

  return {
    props: {
      project,
      article,
    },
    revalidate: REVALIDATE_REPO_INTERVAL,
  };
};

async function fetchProject(projectPath: string, docRoot: string, ref: string) {
  // fetch project properties
  const p1 = fetch(`${apiEndpoint}/repos/${projectPath}`, {
    method: 'GET',
    credentials: 'omit',
  });

  // fetch project file list
  const p2 = fetch(
    `${apiEndpoint}/repos/${projectPath}/git/trees/${ref}:${docRoot.replace(
      /\s/g,
      '%20',
    )}?recursive=1`,
    {
      method: 'GET',
      credentials: 'omit',
    },
  );

  const [r1, r2] = await Promise.all([p1, p2]);

  if (!r1.ok || !r2.ok) return null;

  const [j1, j2] = await Promise.all([
    r1.json(),
    r2.json() as Promise<{ tree: Array<Omit<IFileMeta, 'name'>> }>,
  ]);

  const project = validateProject(j1);

  const files = validateFileMeta(j2.tree.filter((f) => f.type === 'blob'));

  return {
    ...project,
    files,
  };
}

async function fetchFile(projectPath: string, filePath: string[], ref: string) {
  const path = filePath.map((p) => p.replace(/\s/g, '%20')).join('/');

  const url = `https://raw.githubusercontent.com/${projectPath}/${ref}/${path}`;

  const response = await fetch(url, {
    method: 'GET',
    credentials: 'omit',
  });

  if (!response.ok) return null;

  const markdown = await response.text();

  return await transformMarkdownToRemark(markdown);
}
