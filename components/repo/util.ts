import {
  arrayD,
  constantD,
  objectD,
  predicateD,
  stringD,
} from 'ts-decoders/decoders';

export interface IFileMeta {
  // id: number;
  name: string;
  displayName: string;
  path: string;
  type: string;
  url: string;
  sha: string;
}

export type IFileMap = Map<string, IFileMeta | IFileMap>;

export interface IProject {
  id: number;
  name: string;
  url: string;
  files: Array<[string, IFileMeta]>;
}

const STRING_DECODER = stringD().chain(
  predicateD((input: string) => input.length > 0),
);

export const PARAMS_DECODER = objectD({
  projectNamespace: STRING_DECODER,
  projectName: STRING_DECODER,
  refType: STRING_DECODER,
  ref: STRING_DECODER,
  filePath: arrayD(STRING_DECODER),
  root: constantD('docs'),
});

/** Number of seconds to cache repo values for before refetching */
export const REVALIDATE_REPO_INTERVAL = 600;

export const PAGE_NOT_FOUND = {
  notFound: true,
  revalidate: REVALIDATE_REPO_INTERVAL,
} as const;

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export function stringComparer(a: string, b: string) {
  if (a > b) return 1;
  if (a < b) return -1;
  return 0;
}

export function normalizeFileName(name: string) {
  return name.replace(/^[\d].*?(\ |\-)/, '').replace('.md', '');
}

export function normalizeFolderName(name: string) {
  return name.replace(/^[\d].*?(\ |\-)/, '');
}

export function processFiles(array: Array<[string, IFileMeta]>) {
  return array
    .filter(([, f]) => f.name.endsWith('.md'))
    .sort(([, a], [, b]) => stringComparer(a.name, b.name));
}
