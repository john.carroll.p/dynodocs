import { useRouter } from 'next/router';
import Link, { LinkProps } from 'next/link';
import { Children, cloneElement, PropsWithChildren, ReactElement } from 'react';

const stripFragmentFromUrl = (url: string) =>
  encodeURI(url.split('?').shift()!.split('#').shift()!);

export default function ActiveLink(
  props: PropsWithChildren<
    Omit<LinkProps, 'href' | 'as'> & {
      href: string;
      as?: string;
      activeClassName?: string;
    }
  >,
) {
  const { asPath } = useRouter();
  const { children, activeClassName = 'active' } = props;
  const child = Children.only(children) as ReactElement;
  const childClassName = child.props.className || '';

  const baseHref = stripFragmentFromUrl(props.href);
  const currentPath = asPath.split('?').shift()!.split('#').shift()!;
  const propsAs = props.as && stripFragmentFromUrl(props.as);

  // pages/index.js will be matched via props.href
  // pages/about.js will be matched via props.href
  // pages/[slug].js will be matched via props.as
  const className =
    currentPath === baseHref || currentPath === propsAs
      ? `${childClassName} ${activeClassName}`.trim()
      : childClassName;

  return (
    <Link {...props}>
      {cloneElement(child, {
        className: className || null,
      })}
    </Link>
  );
}
