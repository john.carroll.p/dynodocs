import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

export default function useIsRouting() {
  const router = useRouter();
  const [isRouting, setIsRouting] = useState(false);

  useEffect(() => {
    let routeChangeStart = () => setIsRouting(true);
    let routeChangeComplete = () => setIsRouting(false);

    router.events.on('routeChangeStart', routeChangeStart);
    router.events.on('routeChangeComplete', routeChangeComplete);
    router.events.on('routeChangeError', routeChangeComplete);
    return () => {
      router.events.off('routeChangeStart', routeChangeStart);
      router.events.off('routeChangeComplete', routeChangeComplete);
      router.events.off('routeChangeError', routeChangeComplete);
    };
  }, []);

  return isRouting;
}
