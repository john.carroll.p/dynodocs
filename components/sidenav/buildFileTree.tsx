import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import {
  IFileMeta,
  IFileMap,
  normalizeFolderName,
  stringComparer,
} from '../repo/util';
import ActiveLink from '../active-link';

interface IRenderTree {
  path: string;
  name: string;
  children?: IRenderTree[];
}

export function buildFileTree(
  files: Array<[string, IFileMeta]>,
  hrefBase: string,
) {
  const root = 'docs/';

  const fileMap: IFileMap = new Map();

  for (const [, file] of files) {
    const path = file.path.replace(root, '');
    insertIntoFileTree(path, path.split('/'), file, fileMap);
  }

  const firstLevel = Array.from(fileMap.keys());

  return (
    <TreeView
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpanded={firstLevel}
      defaultExpandIcon={<ChevronRightIcon />}
      disableSelection
      className="treeView"
    >
      {renderTree(tranformFileMap(fileMap), hrefBase)}
    </TreeView>
  );
}

function tranformFileMap(map: IFileMap): IRenderTree[] {
  return Array.from(map)
    .sort(([a], [b]) => stringComparer(a, b))
    .map(([, file]) => {
      if (file instanceof Map) {
        const folderPath = (file as any).__path;
        const folderName = (file as any).__name;

        return {
          path: folderPath,
          name: folderName,
          children: tranformFileMap(file),
        };
      }

      return {
        path: file.path,
        name: file.displayName,
      };
    });
}

function insertIntoFileTree(
  path: string,
  segments: string[],
  file: IFileMeta,
  map: IFileMap,
) {
  const segment = segments.shift()!;

  if (segments.length === 0) {
    map.set(segment, file);
  } else {
    if (!map.has(segment)) {
      const fmap = new Map();
      const folderPath = path.split('/');
      folderPath.pop();
      (fmap as any).__path = folderPath.join('/');
      (fmap as any).__name = normalizeFolderName(segment);
      map.set(segment, fmap);
    }

    insertIntoFileTree(path, segments, file, map.get(segment) as IFileMap);
  }
}

function renderTree(nodes: IRenderTree[], hrefBase: string) {
  return nodes.map((node) => {
    if (node.children) {
      return (
        <TreeItem
          key={node.path}
          nodeId={node.path}
          label={node.name}
          className="treeParent"
        >
          {renderTree(node.children, hrefBase)}
        </TreeItem>
      );
    }

    return (
      <ActiveLink key={node.path} href={`${hrefBase}/${node.path}`}>
        <a className="treeChild">
          <TreeItem
            key={node.path}
            nodeId={node.path}
            label={node.name}
          ></TreeItem>
        </a>
      </ActiveLink>
    );
  });
}
