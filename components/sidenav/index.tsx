import { SwipeableDrawer, Hidden } from '@material-ui/core';
import { PropsWithChildren, useMemo } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { IFileMeta } from '../repo/util';
import { useRouter } from 'next/router';
import { buildFileTree } from './buildFileTree';
import useIsRouting from '../use-route-loading';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      [theme.breakpoints.up('md')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    // toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
      padding: '1.5rem 1rem',
      [theme.breakpoints.up('md')]: {
        top: '4rem',
        zIndex: 100,
        height: 'calc(100% - 4rem)',
      },
    },
    // content: {
    //   flexGrow: 1,
    //   padding: theme.spacing(3),
    // },
  }),
);

export default function Sidenav(
  props: PropsWithChildren<{
    hrefBase: string;
    files: Array<[string, IFileMeta]>;
    sidenavOpen: boolean;
    onSidenavToggle: () => void;
  }>,
) {
  const router = useRouter();
  const classes = useStyles();
  const isRouting = useIsRouting();

  const { hrefBase, files, sidenavOpen, onSidenavToggle } = props;

  const fileTree = useMemo(() => {
    if (router.isFallback) return <h1>Loading...</h1>;

    return (
      <TreeView
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpanded={['*']}
        defaultExpandIcon={<ChevronRightIcon />}
        disableSelection
        className="treeView"
      >
        {buildFileTree(files, hrefBase)}
      </TreeView>
    );
  }, [router.isFallback, router.asPath]);

  const noop = () => {};

  if (isRouting && sidenavOpen) {
    onSidenavToggle();
  }

  return (
    <aside className={classes.drawer}>
      <Hidden mdUp implementation="js">
        <SwipeableDrawer
          variant="temporary"
          anchor="left"
          open={sidenavOpen}
          onOpen={noop}
          onClose={onSidenavToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {fileTree}
        </SwipeableDrawer>
      </Hidden>

      <Hidden smDown implementation="js">
        <SwipeableDrawer
          onOpen={noop}
          onClose={onSidenavToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          open
        >
          {fileTree}
        </SwipeableDrawer>
      </Hidden>
    </aside>
  );
}
