import {
  AppBar,
  createStyles,
  CssBaseline,
  IconButton,
  makeStyles,
  Theme,
  Toolbar,
  Typography,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import Link from 'next/link';
import { PropsWithChildren } from 'react';
import { IProject } from '../repo/util';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  }),
);

export default function Header(
  props: PropsWithChildren<{ project: IProject; onSidenavToggle: () => void }>,
) {
  const classes = useStyles();
  const { project, onSidenavToggle } = props;

  return (
    <>
      <CssBaseline />

      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={onSidenavToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            <Link href={project.url}>
              <a>{project.name}</a>
            </Link>
          </Typography>
        </Toolbar>
      </AppBar>
    </>
  );
}
