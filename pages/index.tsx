import { useEffect } from 'react';
import { useRouter } from 'next/router';

const DEFAULT_URL =
  '/d/gitlab/john.carroll.p/rschedule/branch/master/docs/1.%20Introduction.md';

export default function HomePage() {
  const router = useRouter();

  useEffect(() => {
    router.push(DEFAULT_URL);
  }, []);

  return <p>Redirecting...</p>;
}
