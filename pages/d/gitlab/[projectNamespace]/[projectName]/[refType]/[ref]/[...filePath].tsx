export { getStaticPaths } from '@/components/repo/util';
export { getStaticProps } from '@/components/repo/gitlab/getStaticProps';
export { default } from '@/components/repo/gitlab';
