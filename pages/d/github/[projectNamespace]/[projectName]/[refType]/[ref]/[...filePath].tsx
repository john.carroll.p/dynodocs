export { getStaticPaths } from '@/components/repo/util';
export { getStaticProps } from '@/components/repo/github/getStaticProps';
export { default } from '@/components/repo/github';
