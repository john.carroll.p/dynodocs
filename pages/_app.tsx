import '../styles/globals.scss';
import 'node_modules/highlight.js/scss/atom-one-dark.scss';
import { LinearProgress } from '@material-ui/core';
import useIsRouting from '@/components/use-route-loading';

function MyApp({ Component, pageProps }: any) {
  const isRouting = useIsRouting();

  return (
    <>
      {(isRouting && <LinearProgress />) || <></>}

      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
